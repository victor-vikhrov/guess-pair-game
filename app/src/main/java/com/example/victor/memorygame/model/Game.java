package com.example.victor.memorygame.model;

import com.example.victor.memorygame.R;

/**
 * Created by victor on 12.12.17.
 */

public class Game {

    private static final int[] drawables = {
            R.drawable.pack1_chick, R.drawable.pack1_crab,
            R.drawable.pack1_fox, R.drawable.pack1_hedgehog,
            R.drawable.pack1_hippopotamus, R.drawable.pack1_lemur,
            R.drawable.pack1_pig, R.drawable.pack1_tiger
    };

    private Field[][] fields; // объекты игрового поля

    private int stepCount;   // количество ходов игрока

    private int rowCount;
    private int columnCount;

    public Game(int rowCount, int columnCount) {
        this.fields = new Field[rowCount][columnCount];
        this.rowCount = rowCount;
        this.columnCount = columnCount;
        generateGameField();
        mixPictures();
    }

    /**
     * генерация игрового поля, случайным образом
     */
    private void generateGameField() {
        for (int i = 0; i < fields.length; i++) {
            for (int j = 0; j < fields[i].length; j++) {
                fields[i][j] = new Field(drawables[(i * fields.length + j) / 2]);
            }
        }
    }

    /**
     * Перемевание карточек на игровом поле
     */
    private void mixPictures() {
        for (int i = 0; i < 100; i++) {
            int r1 = (int) (Math.random() * rowCount);
            int c1 = (int) (Math.random() * columnCount);
            int r2 = (int) (Math.random() * rowCount);
            int c2 = (int) (Math.random() * columnCount);

            int tmp = fields[r1][c1].getDrawableId();
            fields[r1][c1].setDrawableId(fields[r2][c2].getDrawableId());
            fields[r2][c2].setDrawableId(tmp);
        }
    }

    public Field[][] getFields() {
        return fields;
    }
}
