package com.example.victor.memorygame;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.victor.memorygame.model.Field;

public class GameActivity extends AppCompatActivity implements GameContract.View {

    private static final int DEFAULT_ROW_COUNT = 4;     // количество строк по умолчанию
    private static final int DEFAULT_COLUMN_COUNT = 4;  // количество столбцов по умолчанию

    private LinearLayout rootLayout; // корневой layout для игрового поля
    private ImageView[][] imageViews; // массив ImageView для картинок

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rootLayout = findViewById(R.id.root);
        createUI(DEFAULT_ROW_COUNT, DEFAULT_COLUMN_COUNT);

        GamePresenter presenter = new GamePresenter(this);
        presenter.init(DEFAULT_ROW_COUNT, DEFAULT_COLUMN_COUNT);




    }

    /**
     * Инициализация элементов интерфейса и игрового поля
     * @param rowCount количество рядов в игровом поле
     * @param columnCount количество столбцов в игровом поле
     */
    private void createUI(int rowCount, int columnCount) {
        imageViews = new ImageView[rowCount][columnCount];

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(128, 128); // LayoutParams нужнен для передачи настройки атрибутов View
        params.weight = 1f / columnCount;  // вес каждого ImageView в ряду будет одинаковым
        params.setMargins(16, 16, 16, 16); // отступы для каждого ImageView

        for (int i = 0; i < rowCount; i++) {
            LinearLayout rowLayout = new LinearLayout(this);
            rowLayout.setOrientation(LinearLayout.HORIZONTAL);
            for (int j = 0; j < columnCount; j++) {
                ImageView imageView = new ImageView(this);
                imageView.setLayoutParams(params); // передача атрибутов
                imageViews[i][j] = imageView; // сохранение ссылки на ImageView в массив
                rowLayout.addView(imageView);
            }
            rootLayout.addView(rowLayout); // добавления ряда ImageView в корневой LinearLayout
        }
    }

    @Override
    public void updateGameField(Field[][] fields) {
        for (int i = 0; i < imageViews.length; i++) {
            for (int j = 0; j < imageViews[i].length; j++) {
                imageViews[i][j].setImageResource(fields[i][j].getDrawableId());
            }
        }
    }

    @Override
    public void updateTimer(String time) {

    }

    @Override
    public void updateStepCount(String count) {

    }
}
