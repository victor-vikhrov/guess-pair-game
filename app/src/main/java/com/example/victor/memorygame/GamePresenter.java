package com.example.victor.memorygame;

import android.content.Context;
import android.util.Log;

import com.example.victor.memorygame.model.Game;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * Created by victor on 13.12.17.
 */

public class GamePresenter {

    private Context context;
    private Game game;
    private GameContract.View view;

    public GamePresenter(Context context) {
        this.context = context;
        this.view = (GameContract.View) context;
    }

    public void init(int rowCount, int columnCount) {
        game = new Game(rowCount, columnCount);
        view.updateGameField(game.getFields());
    }


}
