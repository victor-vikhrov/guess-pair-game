package com.example.victor.memorygame.model;

/**
 * Created by victor on 12.12.17.
 */

public class Field {

    private boolean isOpen; // видимость поля для игрока
    private int drawableId; // графический ресурс отображаемый на поле

    public Field(int drawable) {
        this.drawableId = drawable;
    }

    public int getDrawableId() {
        return drawableId;
    }

    public void setDrawableId(int drawableId) {
        this.drawableId = drawableId;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }
}
