package com.example.victor.memorygame;

import com.example.victor.memorygame.model.Field;

/**
 * Created by victor on 13.12.17.
 */

public interface GameContract {

    interface View {
        /**
         * Обновить содержимое ImageView
         * @param fields массив Field объектов, в которых содержится drawableId
         */
        void updateGameField(Field[][] fields);

        /**
         * Обновления таймера
         * @param time Время в формате hh:mm
         */
        void updateTimer(String time);

        /**
         * Обновление кол-ва ходов игрока
         * @param count ходы, которые сделал игрок
         */
        void updateStepCount(String count);
    }

    interface Presenter {
        /**
         * Инициализация игры, игрового поля
         * @param rowCount  количество рядов
         * @param columnCount   количество колонок
         */
        void init(int rowCount, int columnCount);

        /**
         * Игрок нажал на картинку
         * @param rowIndex  индекс ряда картинки
         * @param columnIndex   индекс колонки картинки
         */
        void onImageClick(int rowIndex, int columnIndex);

        /**
         * Закрытие активити
         */
        void onPause();

        /**
         * Открытие активити
         */
        void onResume();
    }
}
